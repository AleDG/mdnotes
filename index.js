'use strict';

/**
 * Converts md files to pdf
 */

// Imports
let md = require('markdown-pdf'),
    fs = require('fs'),
    path = require('path');

// md-pdf options
let options = {
    paperFormat: 'A4',
    paperBorder: '1cm',
    cssPath: path.join(__dirname, 'src/css/pdf.css'),
    highlightCssPath: path.join(__dirname, 'src/css/highlight.css'),
    runningsPath: path.join(__dirname, 'src/js/runnings.js'),
    remarkable: { 
        syntax: [ 'sub', 'sup' ] 
    }
};

// Check if file exists
let _exists = (file) => fs.existsSync(file);

module.exports = function(file, opts, cb) {
    let outD = opts.out,
        cssF = opts.css,
        hdrF = opts.hdr;

    // Check if file exists
    if(!_exists(file)) { 
        return cb(file, null);
    }

    if(_exists(cssF)) {
        options.cssPath = cssF;
    }

    if(_exists(hdrF)) {
        options.runningsPath = hdrF;
    }

    // Convert file
    let out = file
        .split('/')
        .splice(-1)[0]
        .replace(/\.md+$/, '.pdf');
    
    md(options)
        .from(file)
        .to(path.join(outD, out), function() {
            cb(null, file)
        });
};