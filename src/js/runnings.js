exports.header = null

exports.footer = {
    height: "0.5cm",
    contents: function(pageNum, numPages) {
        return "<p style='text-align:center;font-size:.5em;font-family:Dejavu Sans;'>Page " + pageNum + " of " + numPages + "</p>"
    }
}

/**
 * header and footer might be of format specified in http://phantomjs.org/api/webpage/property/paper-size.html
 */
