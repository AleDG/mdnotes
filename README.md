# mdnotes

Markdown to Pdf utility. Basically a wrapper for [markdown-pdf](https://www.npmjs.com/package/markdown-pdf) for batch conversion of Markdown files to Pdf, with preset options and stylesheet.

## Setup

```text
yarn global add https://bitbucket.org/rscolati/mdnotes.git
```

## Usage

```text
usage: mdnotes [-h] [-v] [-o path] [-s path] [--header path] file [file ...]

Positional arguments:
  file                  File to convert

Optional arguments:
  -h, --help            Show this help message and exit.
  -v, --version         Show program's version number and exit.
  -o path, --outdir path
                        Output directory
  -s path, --stylesheet path
                        Custom CSS stylesheet
  --header path         File with custom header and footer
```

### Active Options

```js
{
    paperFormat: 'A4',
    paperBorder: '1cm',
    cssPath:          'src/css/pdf.css',
    highlightCssPath: 'src/css/highlight.css',
    runningsPath:     'src/js/runnings.js',
    remarkable: {
        syntax: [ 'sub', 'sup' ]
    }
}
```

### Stylesheet

See default file [`src/css/pdf.css`](src/css/pdf.css).

### Header/Footer 

See default file [`src/js/runnings.js`](src/js/runnings.js).